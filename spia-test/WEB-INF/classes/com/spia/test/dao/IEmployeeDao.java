package com.spia.test.dao;

import java.util.List;

import com.spia.test.domain.Employee;
import com.spia.test.domain.ResponseDao;

public interface IEmployeeDao {
	
	public List<Employee> getEmployees ();
	
	public Employee getEmployeebyId (String identification);
	
	public ResponseDao setEmployee (Employee employee);
	
	public ResponseDao deleteEmployee (String identification);
	
	

}
