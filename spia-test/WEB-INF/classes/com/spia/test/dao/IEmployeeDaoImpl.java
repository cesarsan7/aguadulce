package com.spia.test.dao;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.spia.test.domain.Employee;
import com.spia.test.domain.ResponseDao;

public class IEmployeeDaoImpl implements IEmployeeDao {
	
	private static  Hashtable<String, Employee> employees = null;
	
	
	public IEmployeeDaoImpl(){
		 if (employees == null) {
			 employees = new Hashtable<String, Employee>();
			 setEmployeeZero();
	     }
	}
	

	public List<Employee> getEmployees() {
		return (new ArrayList<Employee>(employees.values()));
		}

	public Employee getEmployeebyId(String identification) {
		return employees.get(identification);
	}

	public ResponseDao setEmployee(Employee employee) {
		
		ResponseDao responseD = new ResponseDao();
		
		if(employees.containsKey(employee.getIdentification())){
			responseD.setMessage("Empleado con identificacion" + employee.getIdentification() + " Ya existe");
			responseD.setResult("False");
		}else{
			employees.put(employee.getIdentification(), employee);
			responseD.setMessage("Empleado creado con exito");
			responseD.setResult("True");
		}
		
		return responseD;
	}

	public ResponseDao deleteEmployee(String identification) {
        ResponseDao responseD = new ResponseDao();
		
		if(employees.containsKey(identification)){
			employees.remove(identification);
			responseD.setMessage("Empleado eliminado con exito");
			responseD.setResult("True");
		}else{
			responseD.setMessage("Empleado con identificación " + identification + " no existe");
			responseD.setResult("False");
		}
		
		return responseD;
	}
	
	private void setEmployeeZero (){
		
		Employee empleoyeeZero = new Employee();
		empleoyeeZero.setIdentification("97500643");
		empleoyeeZero.setName("Hernan Prieto");
		empleoyeeZero.setAge(33);
		empleoyeeZero.setPosition("Analista TI");
		
		employees.put("97500643", empleoyeeZero);
		
	}

}
