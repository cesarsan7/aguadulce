package com.spia.test.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.spia.test.dao.IEmployeeDao;
import com.spia.test.dao.IEmployeeDaoImpl;
import com.spia.test.domain.Employee;
import com.spia.test.domain.RequestEmployeeService;
import com.spia.test.domain.ResponseDao;
import com.spia.test.domain.ResponseEmployeeService;

@Path("/aguadulce")
public class EmployeeService {

	private IEmployeeDao employeeDao = new IEmployeeDaoImpl();
	
	
	@GET
	@Path("/employees")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEmployeeService getEmployees() {
		
		ResponseEmployeeService rEmployeeService = new ResponseEmployeeService();
		
		rEmployeeService.setResult("True");
		rEmployeeService.setEmployees(employeeDao.getEmployees());
		rEmployeeService.setMessage(null);
		
		
		return rEmployeeService;
		
	}
	
	
	@POST @Consumes("application/json")
	@Path("/employee")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEmployeeService createEmployee(final RequestEmployeeService input) {
		
		return setEmployee(input.getEmployee());
		
	}
	
	
	

	@GET
	@Path("/employee/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEmployeeService getEmployeebyId(@PathParam("id") String identification) {
		
		ResponseEmployeeService rEmployeeService = new ResponseEmployeeService();
		Employee employee = new Employee();
	    
		
		employee = employeeDao.getEmployeebyId(identification);
	
		if (employee ==null){
		    rEmployeeService.setResult("False");
		    rEmployeeService.setMessage("Empleado no existe");
		}else{
			 rEmployeeService.setResult("True");
			 rEmployeeService.setEmployee(employee);
		}
		
		return rEmployeeService;
	}
	
	
	@PUT
	@Path("/employee/{id}")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEmployeeService updateEmployee(@PathParam("id") String identification
			                                      ,final RequestEmployeeService input) {
		
		Employee employee = new Employee();
		ResponseEmployeeService rEmployeeService = new ResponseEmployeeService();
		ResponseDao rDao = new ResponseDao();
	    	
		
		employee = employeeDao.getEmployeebyId(identification);
	
		if (employee ==null){
			
			return setEmployee(input.getEmployee());
		    
		}else{
			 rDao = employeeDao.deleteEmployee(input.getEmployee().getIdentification());
			 
			 if(rDao.getResult().equals("True"))
			 {
				rEmployeeService = setEmployee(input.getEmployee());
				rEmployeeService.setMessage("Empleado actualizado con exito");
				
				return rEmployeeService;
			 }else{
				 rEmployeeService.setResult("False");
				 rEmployeeService.setMessage("Error actualizando empleado");
				 return rEmployeeService;
			 }
		}
		
	}
	
	
	@DELETE
	@Path("/employee/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEmployeeService deleteEmployee(@PathParam("id") String identification) {
		
		Employee employee = new Employee();
		ResponseEmployeeService rEmployeeService = new ResponseEmployeeService();
		ResponseDao rDao = new ResponseDao();
	    	
		
		employee = employeeDao.getEmployeebyId(identification);
	
		if (employee ==null){
			
			 rEmployeeService.setResult("False");
			 rEmployeeService.setMessage("No existe empleado");
			 return rEmployeeService;
		    
		}else{
			 rDao = employeeDao.deleteEmployee(identification);
			 
			 if(rDao.getResult().equals("True"))
			 {
				 rEmployeeService.setResult("True");
				 rEmployeeService.setMessage("Empleado eliminado con exito");
				
				return rEmployeeService;
			 }else{
				 rEmployeeService.setResult("False");
				 rEmployeeService.setMessage("Error al eliminar empleado");
				 return rEmployeeService;
			 }
		}
		
	}
	
	
	private ResponseEmployeeService setEmployee(Employee employee){
		
        ResponseEmployeeService rEmployeeService = new ResponseEmployeeService();
		
	    ResponseDao rDao = new ResponseDao();
	    
	    
	    rDao = employeeDao.setEmployee(employee);
		rEmployeeService.setResult(rDao.getResult());
		rEmployeeService.setMessage(rDao.getMessage());
		
		return rEmployeeService;
	
	}
	
	
	
}
