var app = angular.module('myApp', ['angularUtils.directives.dirPagination']);

app.controller('appController', function($scope, $http) {

    $scope.selection=[];
    $scope.serverPath = "http://localhost:8080/";

    $scope.toggleSelection = function toggleSelection(plaque) {
        var index = $scope.selection.indexOf(plaque);
        // is currently selected
        if (index > -1) {
            $scope.selection.splice(index, 1);
        }
        // is newly selected
        else{
            $scope.selection.push(plaque);
        }
    };

    // for table sorting
    $scope.sort = function(keyname) {
        $scope.sortKey = keyname;   // set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; // if true make it false and vice versa
    }

    $scope.getAll = function() {
        $http.get($scope.serverPath + "spia-test/aguadulce/employees").success(function(response) {
            if (response.employees!=undefined) {
                $scope.employees = response.employees;
            }
            else{
                Materialize.toast("No se encontraron empleados.", 4000);
            }
        });
    };

    $scope.create = function() {
        var employee =
            {
                'identification' : $scope.employee.identification,
                'name' : $scope.employee.name,
                'age' : $scope.employee.age,
                'position' : $scope.employee.position
            };
        $http.post($scope.serverPath + "spia-test/aguadulce/employee/", {
             'employee' : employee
            }
        ).success(function (data, status, headers, config) {
            Materialize.toast(data, 4000);
            $('#modal-employees-form').closeModal();
            $scope.clearForm();
            $scope.getAll();
        });
    };

 
    $scope.readOne = function(identification) {
        $('#modal-employees-title').text("Editar");
        $('#btn-update-employees').show();
        $('#btn-create-employees').hide();

        $http.get($scope.serverPath + 'spia-test/aguadulce/employee/' + identification)
        .success(function(data, status, headers, config) {
            console.log(data);
            $scope.employee = data;
            $('#modal-employees-form').openModal();
        })
        .error(function(data, status, headers, config) {
            Materialize.toast('No fué posible obtener la información solicitada.', 4000);
        });
    };

    $scope.update = function() {
        var employee =
            {
                'identification' : $scope.employee.identification,
                'name' : $scope.employee.name,
                'age' : $scope.employee.age,
                'position' : $scope.employee.position
            };
        $http.put($scope.serverPath + 'spia-test/aguadulce/employee?', {
            'employee' : employee
        })
        .success(function (data, status, headers, config) {
            Materialize.toast(data, 4000);
            $('#modal-employees-form').closeModal();
            $scope.clearForm();
            $scope.getAll();
        });
    };

    $scope.delete = function(identification) {
        if (confirm("Desea eliminar el registro?")) {
            $http.delete($scope.serverPath + 'spia-test/aguadulce/employee/' + identification)
                .success(function (data, status, headers, config) {
                    Materialize.toast(data, 4000);
                    $scope.getAll();
            });
        }
    };

    $scope.showCreateForm = function() {
        $scope.clearForm();
        $scope.isActive = true;
        $('#modal-employees-title').text("Nuevo Empleado");
        $('#btn-update-employees').hide();
        $('#btn-create-employees').show();
    };

    $scope.clearForm = function() {
        if ($scope.employee != undefined) {
            $scope.employee.identification = "";
            $scope.employee.name = "";
            $scope.employee.age = "";
            $scope.employee.position = "";
        }
    };
});
